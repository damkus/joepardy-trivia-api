from flask import Flask
from flask_restful import Api, Resource, reqparse
import psycopg2
from config import config
import json


app = Flask(__name__)
api = Api(app)


class Question(Resource):

	def get(self, category, amount, difficulty):
		conn = None
		try:
			category = category.upper()

			params = config()

			print('Connecting to the Trivia Database...')
			conn = psycopg2.connect(**params)

			cur = conn.cursor()

			print('Executing query...')
			cur.execute('SELECT count(category) FROM all_questions WHERE category=%s;', (category,))
			category_count = cur.fetchone()
			if category_count[0] == 0:
				return "No categories found", 400
			if category_count[0]<amount:
				return "Requested amount too big", 400
			if difficulty == 'easy':
				cur.execute("SELECT category, question, answer \
				FROM all_questions \
				WHERE (category=%s) \
				AND (value='$100' OR value='$200' OR value='$300' OR value='$400' OR value='$500' OR value='$600') \
				ORDER BY RANDOM() LIMIT %s;", \
				(category, amount))
			elif difficulty == 'medium':
				cur.execute("SELECT category, question, answer \
				FROM all_questions \
				WHERE (category=%s) \
				AND (value='$700' OR value='$800' OR value='$900' OR value='$1000' OR value='$1,000') \
				ORDER BY RANDOM() LIMIT %s;", \
				(category, amount))
			elif difficulty == 'hard':
				cur.execute("SELECT category, question, answer \
				FROM all_questions \
				WHERE (category=%s) \
				AND (value='$1,100' OR value='$1200' OR value='$1,200' OR value='$1,300' OR value='$1,400' OR value='$1,500' OR value='$1600' OR value='$1,600' OR value='$1,800' OR value='$2000' OR value='$2,000' OR value='$2,500') \
				ORDER BY RANDOM() LIMIT %s;", \
				(category, amount))
			else:
				return "Difficulty not found", 400
			response = cur.fetchall()
			cur.close()
			if response:
				return json.dumps(response), 200
			return "API Error", 400
		except (Exception, psycopg2.DatabaseError) as error:
			return error, 404
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

	def post(self):
		return 'Only GET is allowed', 404

	def put(self):
		return 'Only GET is allowed', 404

	def delete(self):
		return 'Only GET is allowed', 404


api.add_resource(Question, "/api/category=<string:category>&amount=<int:amount>&difficulty=<string:difficulty>")
app.run(port=8080)
